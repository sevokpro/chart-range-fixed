(function () {
    angular.module('app').directive('myChartrangefixed', [function () {
        return {
            templateUrl: '/js/angular/components/MyChartRangeFixed/chart-range.html',
            replace: true,
            restrict: 'E',
            scope: {
            	data: '@',
                uplink: '@'
            },
            controller: function($scope, $element, $attrs, $http) {

            	var agent = navigator.userAgent;
				var ie = (/MSIE/).test(agent) || (/Trident/).test(agent);

            	//init object
				$scope.slider = {};
			
				//init data
				$scope.slider.uplink = $scope.uplink;

				//init ranges
				$scope.slider.ranges = {
					min: 0,
					max: 100
				};
				
				$scope.slider.get_data = function () {
					var data = $scope.data;
					data =JSON.parse(data);
					return data
				}
				
				$scope.slider.data = $scope.slider.get_data();

				//set DOM element
				$scope.slider.DOM_link = $($element).children('#slider');

				//get tooltip template
				$scope.slider.get_tooltip_template = function (index) {
					var tooltip_tmpl = [
					'<span class="tooltip-wrapper">',
						'<input ',
							'type="number" ',
							'class="range-chart-tooltip ',(ie) ? 'explorer-tooltip-support' : '','" ',
							'value="',$scope.slider.data[index],'"',
						' />',
						'<div class="tooltip-after ',(ie) ? 'explorer-after-support' : '','"></div>',
					'</span>'
						].join('');
					return tooltip_tmpl;
				};

				//get point template
				$scope.slider.get_point_template = function (index) {
					var point_template = [
						'<span class="point">',
						index + 1,
						'</span>',
						$scope.slider.get_tooltip_template(index)
						].join('');
					return point_template;
				};		

				//function-constructor for slider
				$scope.slider.create = function (event, ui) {

					//get slider points
					var childrens = $($scope.slider.DOM_link).children('.ui-slider-handle');

					//correct slider oints, create tootlips for each point
					for (var i = 0; i < childrens.length; i++){
						//correct text on point
						$(childrens[i]).css({
							'text-align': 'center'
						});
						//create tooltip
						$(childrens[i]).html(
							$scope.slider.get_point_template(i)
							);
					}

					$scope.slider.DOM_link.append('<div style="position:absolute; top: -15px">0</div>')
					$scope.slider.DOM_link.append('<div style="position:absolute; top: -15px; right: 0px;">100</div>')

					//add event listner for change tooltip
					$($scope.slider.DOM_link).on('keyup change', 'input', $scope.slider.tooltip_change_event);
				};

				$scope.slider.tooltip_change_event = function (e) {
					//get new data
					new_data = $scope.slider.coollect_data_from_tooltips();
					//check new data for errors
					for (var i = 0; i < new_data.length - 1; i++) {
						if (new_data[i] >= new_data[i + 1]){
							console.log('ошибка!');
							return false;
						}
					}
					//var new_ranges = $scope.slider.calculate_ranges(new_data);
					//update clider data if new data correct

					$($scope.slider.DOM_link).slider( "option", "values", new_data );
				};

				//collect data from tooltips
				$scope.slider.coollect_data_from_tooltips = function () {
					var out = [];
					var childrens = $($scope.slider.DOM_link).find('input');
					for (var i = 0; i < childrens.length; i++) {
						out.push(+$(childrens[i]).val());
					}
					return out;
				};

				//function which check points for correct input 
				//!!! and update tooltip
				$scope.slider.slide_move = function (event, ui) {
					//check new values
					for (var i = 0; i < ui.values.length - 1; i++) {
						if (ui.values[i] > ui.values[i + 1]){
							return false;
						}
					}
					//update tooltip
			    	var elem = $(ui.handle).find('input');
			    	elem.val(ui.value);	
				};

				$scope.slider.save = function () {
					var data = $($scope.slider.DOM_link).slider( "option", "values");
					//data = JSON.stringify(data);

					var req = {
                        method: 'POST',
                        url: $scope.slider.uplink,
                        headers: {
                            'Content-Type': 'JSON'
                        },
                        data: {
                            d: JSON.stringify(data) 
                        }
                    }

                    $http(req).then(function successCallback(response) {
                        console.log('данные успешно отправлены на сервер');
                    }, function errorCallback(response) {
                        console.log('при отправке данных произошла ошибка');
                        console.log(response)
                    });
				};

                //init slider
                $($scope.slider.DOM_link).slider({
					min: $scope.slider.ranges.min,
					max: $scope.slider.ranges.max,
					step: 1,
					animate: "slow",
					values: $scope.slider.data,
					create: $scope.slider.create,
					slide: $scope.slider.slide_move
				});
            },
            // compile: function compile(tElement, tAttrs, transclude) {
            //     return function postLink(scope, iElement, iAttrs, controller) {
    
            //     }
            // },
            link: function postLink(scope, iElement, iAttrs) {
                
            }
        };
    }]);
})(); 